Title: Customizing your Journal
Date: 2020-06-05

Journal Generator comes with a few themes you can pick out of the box. They are:

* `glacier`: a more cold look, light blue background with gray text.
* `glacier-inv`: inverted version of `glacier`
* `forest`: The default look of Journal Generator, dark gray text on light green background.
* `forest-inv`: inverted `forest`
* `lemonbar`: mak


