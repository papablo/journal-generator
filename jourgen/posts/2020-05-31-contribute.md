Title: Contribute
Date: 2020-05-31

Would you like to add new features to Journal Generator?  Feel free to do so!

Fork the [project on Gitlab](https://gitlab.com/papablo/journal-generator). Make a merge request or create and [issue](https://gitlab.com/papablo/journal-generator/-/issues/new) requesting that functionality.
