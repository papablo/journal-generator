Title: Getting Started
Date: 2020-06-10

Start writing your journal is really easy! All you have to do is:

* Download [starter template]() from our repository.
* Unzip it to a location of your choice.
* Install all dependencies with: `pip install -r requirements.txt` 
    * You might want to use a [Virtualenv](https://www.pythonforbeginners.com/basics/how-to-use-python-virtualenv/).
* Run `python serve` and voila! You have an example journal (this one) running in `http://localhost:5500`
