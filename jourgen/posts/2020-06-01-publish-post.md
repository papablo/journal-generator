Title: How to publish your Journal
Date: 2020-06-01

So, you have written a few posts and want the world to know them? You cando that.

First, you need to create a finished version of your journal, you can do that running:

```python
python app.py build
```

After that, your journal is ready to be deployed, just make your that your `site` folder is being served.

And that's it!
